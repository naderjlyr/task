import Vue from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth/index'
import { LOADING_SPINNER_SHOW_MUTATION } from './storeconstants'
Vue.use(Vuex)

export default new Vuex.Store({
  state () {
    return {
      showLoading: false
    }
  },
  mutations: {
    [LOADING_SPINNER_SHOW_MUTATION] (state, payload) {
      state.showLoading = payload
    }
  },
  actions: {},
  modules: { auth }
})
