import axios from 'axios'
import store from '@/store/index'
import { GET_USER_TOKEN_GETTER } from '@/store/storeconstants'
const axiosInstance = axios.create({})

axiosInstance.interceptors.request.use((config) => {
  const params = new URLSearchParams()
  const token = store.getters[`auth/${GET_USER_TOKEN_GETTER}`]
  params.append('auth', token)
  config.params = params
  return config
})

export default axiosInstance
