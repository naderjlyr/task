import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index'
import { IS_USER_AUTHENTICATE_GETTER } from '@/store/storeconstants'
import Login from '@/views/Login'
import Dashboard from '@/views/Dashboard'
import Charts from '@/views/Charts'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: { auth: false }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: { auth: true }
  },
  {
    path: '/charts',
    name: 'charts',
    component: Charts,
    meta: { auth: true }
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})
router.beforeEach((to, from, next) => {
  if (to.meta.auth && !store.getters[`auth/${IS_USER_AUTHENTICATE_GETTER}`]) {
    next('/')
  } else if (
    !to.meta.auth && store.getters[`auth/${IS_USER_AUTHENTICATE_GETTER}`]
  ) {
    console.log(to)
    next('/dashboard')
  } else if (
    !to.meta.auth && store.getters[`auth/${IS_USER_AUTHENTICATE_GETTER}`]
  ) {
    next('/charts')
  } else {
    next()
  }
})
export default router
